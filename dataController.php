<?php

$log_file='search.log';

function saveData($address){
    global $log_file;

        $file = fopen($log_file, 'a+');
        $log_time = date("d-m-Y H:i:s");
        $record = $log_time.";".$_SERVER['REMOTE_ADDR'].";".$address."\n";

        $record .= file_get_contents($log_file);
        file_put_contents($log_file, $record);
        fclose($file);
    return true;
}


function readLogData(){
    global $log_file;

    if(file_exists($log_file)) {
        $file = fopen($log_file, 'r') or die("не удалось открыть файл");
        $i=1;
            while (!feof($file)) {

                $str = htmlentities(fgets($file));
                $str1 = explode(';', $str);

                echo "<div class='row".$i."'>";
                    echo "<div class='col-3'>" . $str1[0] . "</div>";
                    echo "<div class='col-3'>" . $str1[1] . "</div>";
                    echo "<div class='col-6'>" . $str1[2] . "</div>";
                echo "</div>";
                $i='';
            }
        fclose($file);
    }
    return true;
}

function showTest($num){
    unlink('search_test.log');
    $i=0;
    $start = microtime(true);
    while ($i<>$num) {
        $chars_req = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $random_str = substr(str_shuffle($chars_req), 3, 15);

        $file = fopen('search_test.log', 'a+');
        $log_time = date("d-m-Y H:i:s");
        $record = $log_time . ";" . $_SERVER['REMOTE_ADDR'] . ";" . $random_str;

        fwrite($file,$record."\r\n");
        fclose($file);
        $i++;
    }
    echo "<div class='title-center'>";
        echo $num." запросов отработали за:<br><strong>".$time = microtime(true) - $start."</strong> секунд.";
        echo "Посмотреть лог запросов:<br><a href='search_test.log' target='_blank'>Serach_test.log</a>";
    echo "</div>";
}



?>